# Battery health notifications

Scientific research tells us lithium-ion batteries have a longer life when
their charge doesn't go below 30% and above 80%. This simple and tiny script
helps you accomplish this.

You'll get a notification when:

- Your cable is unplugged and the battery goes below 30%
- Your cable is plugged and the battery goes above 80%

The notification *don't* automatically go away when you plug/unplug the power
cable. You'll need to click them to make them disappear.

## Install

Save the script somewhere on your drive, then add the script to your crontab
(`crontab -e`):

    */5 * * * * /bin/bash /path/to/battery_health_notifications.sh

Or use this:

	cd ~/.local/
    wget https://gitlab.com/gitaarik/battery-health-notifications/-/raw/master/battery_health_notifications.sh
    crontab -l > mycrontab
    echo "*/5 * * * * /bin/bash $HOME/.local/battery_health_notifications.sh" >> mycrontab
    crontab mycrontab
    rm -f mycrontab


## Development

It's been tested on Ubuntu 19.04 running GNOME 3.32.1. Contributions to make it
work on more environments are welcome.
